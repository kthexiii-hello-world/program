$$
f = 2
$$

```math
\frac{2}{2}
```

```math
\begin{cases}
  y'' + \frac{c}{mL}y' + \frac{g}{L}\sin y = 0 \\
  y(0) = a \\
  y'(0) = b
\end{cases}
```
